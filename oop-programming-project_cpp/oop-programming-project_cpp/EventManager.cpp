#include "EventManager.h"
#include <vector>
#include <iostream>
#include "Event.h"

EventManager::EventManager(std::vector<Event*> e)
{
	Event *event1 = new Event("CreateGame", "Create a new video game for your company.", 1);
	Event *event2 = new Event("HireEmployee", "Hire a new employee for your company.", 2);

	e.push_back(event1);
	e.push_back(event2);

	events = e;
}


EventManager::~EventManager()
{
	for (int i = 0; i < events.size(); i++)
	{
		delete events.at(i);
	}
}

void EventManager::PrintEvents()
{
	for (int i = 0; i < events.size(); i++)
	{
		std::cout << "\n";
		events.at(i)->print();
	}
}

Game* EventManager::CreateGame(std::string name, std::string details, int id, float price, Devkit* devkit, bool multiplayer, std::string engine)
{
	Game* newGame = new Game(name, details, id, price, devkit, multiplayer, engine);
	return newGame;
}
