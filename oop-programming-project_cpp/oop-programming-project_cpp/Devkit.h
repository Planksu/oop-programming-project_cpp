#pragma once
#include "Item.h"
#include <string>
class Devkit : public Item
{
public:
	Devkit(std::string n, std::string d, int i, float p, float pm, int pa);
	Devkit();
	~Devkit();
	void Print();
	float GetProfitMultiplier() const { return profitMultiplier; }
	int GetPlayerAmount() const { return playerAmount; }
	void SetProfitMultiplier(float newProfitMultiplier);
	void SetPlayerAmount(int newPlayerAmount);

protected:
	float profitMultiplier;
	int playerAmount;
};

