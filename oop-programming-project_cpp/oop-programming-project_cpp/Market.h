#pragma once
#include <vector>
#include "Devkit.h"
#include "Character.h"
class Market
{
public:
	Market(std::vector<Devkit*> d, std::vector<Character*> e);
	Market();
	~Market();
	void PrintDevkits();
	void PrintEmployees();
	Devkit* GetDevkit(int devkitID);
	Character* GetEmployee(int employeeID);

	std::vector<Devkit*> devkits;
	std::vector<Character*> employees;
};

