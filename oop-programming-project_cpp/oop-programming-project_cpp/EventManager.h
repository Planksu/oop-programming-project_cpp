#pragma once
#include <vector>
#include "Event.h"
#include "Devkit.h"
#include "Game.h"
class EventManager
{
public:
	EventManager(std::vector<Event*> e);
	~EventManager();
	void PrintEvents();
	Game* CreateGame(std::string name, std::string details, int id, float price, Devkit* devkit, bool multiplayer, std::string engine);

protected:
	std::vector<Event*> events;
};

