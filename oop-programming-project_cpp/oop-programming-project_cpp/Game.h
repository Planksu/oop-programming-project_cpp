#pragma once
#include "Item.h"
#include <string>
#include "Devkit.h"

class Game : public Item
{
public:
	Game(std::string n, std::string d, int i, float p, Devkit* de, bool mp, std::string en);
	~Game();

	void Print();
	float CalculateProfits();

	std::string getEngine() const { return engine; }
	bool getMultiplayer() const { return multiplayer; }

	Devkit* devkit;
protected:
	bool multiplayer;
	std::string engine;
};

