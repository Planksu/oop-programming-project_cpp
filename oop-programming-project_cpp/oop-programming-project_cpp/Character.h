#pragma once
#include "GameObject.h"
#include <string>
class Character : public GameObject
{
public:
	Character(std::string n, std::string d, int i, int pS, int gS, int mS, int eY, int pay);
	Character(Character& other);
	Character();
	~Character();
	void Print();
	int GetProgrammingSkills() const { return programmingSkills; }
	int GetGraphicsSkills() const { return graphicsSkills; }
	int GetMarketingSkills() const { return marketingSkills; }
	int GetExperienceYears() const { return experienceYears; }
	int GetRequiredPay() const { return requiredPay; }
	void SetProgrammingSkills(int newProgrammingSkills);
	void SetGraphicsSkills(int newGraphicsSkills);
	void SetMarketingSkills(int newMarketingSkills);
	void SetExperienceYears(int newExperienceYears);

protected:
	int programmingSkills;
	int graphicsSkills;
	int marketingSkills;
	int experienceYears;
	int requiredPay;
};

