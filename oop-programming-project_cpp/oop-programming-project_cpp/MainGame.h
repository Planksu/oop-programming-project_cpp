#pragma once
#include "EventManager.h"
#include "Character.h"
#include "Market.h"
#include <vector>
#include "FileHandler.h"
class MainGame
{
public:
	MainGame(EventManager* e, std::vector<Character*> em, std::vector<Devkit*> de, std::vector<Game*> ga, int c, Market m, FileHandler* fh);
	~MainGame();
	void Start();
	void OpenMainMenu();
	void PrintEvents();
	void PrintEmployees();
	void PrintGames();
	void UpdateEmployeeInfo();
	std::string GetInputValueString();
	int GetInputValueInt();
	void PrintMarketItems();
	int CreateGame();
	void CheckPurchase();
	void CalculateProfits(int gameID);
	void Purchase(std::vector<Character*> marketVector, std::vector<Character*> destinationVector);
	void Purchase(std::vector<Devkit*> marketVector, std::vector<Devkit*> destinationVector);

protected:
	EventManager *eventManager;
	std::vector<Character*> employees;
	std::vector<Devkit*> devkits;
	std::vector<Game*> games;
	Market market;
	int cash;
	FileHandler *filehandler;
};

