#include "MainGame.h"
#include "EventManager.h"
#include "Market.h"
#include <iostream>
#include <vector>
#include <stdlib.h>

MainGame::MainGame(EventManager* e, std::vector<Character*> em, std::vector<Devkit*> de, std::vector<Game*> ga, int c, Market m, FileHandler* fh)
{	
	fh = new FileHandler("games.txt", "devkits.txt", "employees.txt", "cash.txt");
	filehandler = fh;
	eventManager = e;
	employees = filehandler->loadEmployees();
	devkits = filehandler->loadDevkits();
	games = filehandler->loadGames();
	cash = filehandler->loadCash();
	market = m;
}


MainGame::~MainGame()
{
	
}

void MainGame::Start() 
{
	OpenMainMenu();
}

void MainGame::OpenMainMenu()
{		
	int Input;
	int GameID;
	do 
	{
			std::cout << "\n\nWelcome to the game development game!\n" << std::endl;
			std::cout << "1) Show current events" << std::endl;
			std::cout << "2) Show current employees" << std::endl;
			std::cout << "3) Show created games" << std::endl;
			std::cout << "4) Update employee information" << std::endl;
			std::cout << "5) Show available items and employees in the market" << std::endl;
			std::cout << "6) Create a game!" << std::endl;
			std::cout << "0) Stop playing" << "\n" << std::endl;
			std::cout << "Current balance: " << cash << std::endl;
			std::cin >> Input;

			system("cls");

			switch (Input)
			{
			case 1:
				PrintEvents();
				break;
			case 2:
				PrintEmployees();
				break;
			case 3:
				PrintGames();
				break;
			case 4:
				UpdateEmployeeInfo();
				break;
			case 5:
				PrintMarketItems();
				CheckPurchase();
				break;
			case 6:
				GameID = CreateGame();
				CalculateProfits(GameID);
				break;
			case 0:
				break;
			default:
				std::cout << "Incorrect input!" << std::endl;
				break;
			} 

	} 
	while (Input != 0);
}

void MainGame::PrintEvents()
{
	eventManager->PrintEvents();
}

void MainGame::PrintGames()
{
	for (int i = 0; i < games.size(); i++)
	{
		games.at(i)->Print();
	}
}

int MainGame::CreateGame()
{
	float requiredPay = 0;
	//calculate the required money to make a game (a month's pay for all employees in this case)
	for (int i = 0; i < employees.size(); i++)
	{
		requiredPay += employees.at(i)->GetRequiredPay();
	}

	//if you have less cash than the required amount, can't make a game can you?
	if (requiredPay > cash) 
	{
		std::cout << "You don't have enough cash to make a game!" << std::endl;
		return 0;
	}

	std::string name;
	std::string details;
	int id;
	float price;
	Devkit* devkit = new Devkit;
	bool multiplayer;
	std::string engine;

	std::cin.ignore();
	std::cout << "Enter a name for the game: " << std::endl;
	std::getline(std::cin, name);

	std::cout << "Enter details for the game: " << std::endl;
	std::getline(std::cin, details);

	std::cout << "Enter a id for the game: " << std::endl;
	std::cin >> id;

	std::cout << "Enter a price to sell the game: " << std::endl;
	std::cin >> price;

	std::cout << "Which devkit would you like to use? Enter a id:" << std::endl;

	for (int i = 0; i < devkits.size(); i++)
	{
		devkits.at(i)->Print();
	}		
	int input;
	std::cin >> input;
	for (int i = 0; i < devkits.size(); i++)
	{
		if (input == devkits.at(i)->getId())
		{
			devkit = devkits.at(i);
		}
	}

	std::cout << "Is your game a multiplayer? Enter 1 for yes, 0 for no." << std::endl;
	int multiplayerInput;
	std::cin >> multiplayerInput;

	if (multiplayerInput == 1)
	{
		multiplayer = true;
	}
	else if(multiplayerInput == 0)
	{
		multiplayer = false;
	}

	std::cin.ignore();
	std::cout << "Enter the engine used to create this game: " << std::endl;
	std::getline(std::cin, engine);

	//after the games attributes have been assigned their values, create a game object and add it to the vector of games
	games.push_back(eventManager->CreateGame(name, details, id, price, devkit, multiplayer, engine));

	//return the id since the id is used to find the game from the games list for profit calculation
	return id;
}

void MainGame::CalculateProfits(int gameID)
{
	for (int i = 0; i < games.size(); i++)
	{
		if (games.at(i)->getId() == gameID)
		{
			cash += games.at(i)->CalculateProfits();
		}
	}
}

void MainGame::PrintEmployees()
{
	for (int i = 0; i < employees.size(); i++)
	{
		employees.at(i)->Print();
	}
}

void MainGame::PrintMarketItems()
{
	market.PrintDevkits();
	market.PrintEmployees();
}

void MainGame::CheckPurchase()
{
	int input;
	std::cout << "Would you like to buy something/hire someone? Enter 1 for item purchase and 2 for employee hiring or 0 for exit." << std::endl;
	std::cin >> input;

	switch (input)
	{
	case 0:
		break;
	case 1:
		Purchase(market.employees, employees);
		break;
	case 2:
		Purchase(market.devkits, devkits);
		break;
	}

}

void MainGame::Purchase(std::vector<Character*> marketVector, std::vector<Character*> destinationVector)
{
	int id;
	std::cout << "Enter the id: " << std::endl;
	std::cin >> id;

	for (int i = 0; i < marketVector.size(); i++)
	{
		if (id == marketVector[i]->getId())
		{
			destinationVector.push_back(marketVector[i]);
			marketVector.erase(marketVector.begin() + i);
		}
	}
}

void MainGame::Purchase(std::vector<Devkit*> marketVector, std::vector<Devkit*> destinationVector)
{
	int id;
	std::cout << "Enter the id: " << std::endl;
	std::cin >> id;

	for (int i = 0; i < marketVector.size(); i++)
	{
		if (id == marketVector[i]->getId())
		{
			destinationVector.push_back(marketVector[i]);
			marketVector.erase(marketVector.begin() + i);
		}
	}
}


void MainGame::UpdateEmployeeInfo()
{
	int input;
	Character *employeeToModify = new Character();
	std::cout << "Enter the employee ID whose info you would like to modify: " << std::endl;
	std::cin >> input;

	for (int i = 0; i < employees.size(); i++)
	{
		if (employees.at(i)->getId() == input)
		{
			employeeToModify = new Character(*employees.at(i));
		}
	}

	std::cout << "What info would you like to modify?" << std::endl;

	std::cout << "1) Name" << std::endl;
	std::cout << "2) Description" << std::endl;
	std::cout << "3) ID" << std::endl;
	std::cout << "4) Programming skills" << std::endl;
	std::cout << "5) Graphics skills" << std::endl;
	std::cout << "6) Marketing skills" << std::endl;
	std::cout << "7) Experience years" << std::endl;
	std::cin >> input;

	switch(input)
	{
	case 1: 
		employeeToModify->SetName(GetInputValueString());
		break;
	case 2:
		employeeToModify->SetDetails(GetInputValueString());
		break;
	case 3:
		employeeToModify->SetID(GetInputValueInt());
		break;
	case 4:
		employeeToModify->SetProgrammingSkills(GetInputValueInt());
		break;
	case 5:
		employeeToModify->SetGraphicsSkills(GetInputValueInt());
		break;
	case 6:
		employeeToModify->SetMarketingSkills(GetInputValueInt());
		break;
	case 7:
		employeeToModify->SetExperienceYears(GetInputValueInt());
		break;
	default:
		std::cout << "Bad input" << std::endl;
	}

}

std::string MainGame::GetInputValueString()
{
	std::string output;

	std::cout << "Enter new value: " << std::endl;
	std::cin >> output;

	return output;
} 

int MainGame::GetInputValueInt()
{
	int output;

	std::cout << "Enter new value: " << std::endl;
	std::cin >> output;

	return output;
}