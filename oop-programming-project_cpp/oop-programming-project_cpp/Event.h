#pragma once
#include <string>
class Event
{
public:
	Event(std::string n, std::string d, int i);
	Event();
	Event(Event& other);
	~Event();
	void print();
	std::string GetName() const { return name; }
	std::string GetDesc() const { return description; }
	int GetId() const { return id; }
	void SetName(std::string newName);
	void SetDesc(std::string newDesc);
	void SetId(int newId);

protected:
	std::string name;
	std::string description;
	int id;
};

