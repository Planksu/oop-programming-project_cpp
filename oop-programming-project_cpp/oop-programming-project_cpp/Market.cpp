#include "Market.h"
#include <vector>
#include <string>
#include <iostream>


Market::Market(std::vector<Devkit*> d, std::vector<Character*> e)
{
	Devkit *kit1 = new Devkit("PS4 Devkit", "A development kit for the PlayStation 4 platform", 1, 10000, 1.5, 20000000);
	Devkit *kit2 = new Devkit("Xbox One Devkit", "A development kit for the Xbox One platform", 2, 8000, 1.5, 15000000);
	d.push_back(kit1);
	d.push_back(kit2);

	devkits = d;

	Character *employee1 = new Character("Jack", "An artist. Worked in a small indie game development company before it went down", 3, 1, 7, 3, 3, 2300);
	Character *employee2 = new Character("Hannah", "A programmer, specialised in making console games", 4, 9, 0, 4, 10, 2700);
	e.push_back(employee1);
	e.push_back(employee2);

	employees = e;
}

Market::Market()
{
	std::vector<Devkit*> d;
	devkits = d;

	std::vector<Character*> e;
	employees = e;
}

Market::~Market()
{
}

void Market::PrintDevkits()
{
	for (int i = 0; i < devkits.size(); i++)
	{
		devkits.at(i)->Print();
	}
}

void Market::PrintEmployees()
{
	for (int i = 0; i < employees.size(); i++)
	{
		employees.at(i)->Print();
	}
}

Devkit* Market::GetDevkit(int devkitID)
{
	for (int i = 0; i < devkits.size(); i++)
	{
		if (devkits.at(i)->getId() == devkitID)
		{
			return devkits.at(i);
		}
	}
}

Character* Market::GetEmployee(int employeeID)
{
	for (int i = 0; i < employees.size(); i++)
	{
		if (employees.at(i)->getId() == employeeID)
		{
			return employees.at(i);
		}
	}
}