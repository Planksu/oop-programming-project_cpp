#pragma once
#include <string>
class GameObject
{
public:
	GameObject(std::string n, std::string d, int i);
	GameObject();
	GameObject(GameObject& other);
	virtual ~GameObject();
	virtual void Print() = 0;
	std::string getName() const { return name; }
	std::string getDetails() const { return details; }
	int getId() const { return id; }
	void SetName(std::string newName);
	void SetDetails(std::string newDetails);
	void SetID(int newId);

protected:
	std::string name;
	std::string details;
	int id;
};

