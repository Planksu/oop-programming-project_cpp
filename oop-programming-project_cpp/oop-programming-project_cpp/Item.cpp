#include "Item.h"
#include <string>
#include <iostream>

Item::Item(std::string n, std::string d, int i, float p) : GameObject(n, d, i)
{
	price = p;
}

Item::Item() : GameObject(" ", " ", 0)
{
	price = 0;
}

Item::~Item()
{
}

void Item::SetPrice(float newPrice)
{
	price = newPrice;
}

void Item::Print()
{
	GameObject::Print();
	std::cout << "Price: " << price << std::endl;
}