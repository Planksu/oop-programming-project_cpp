#include "Devkit.h"
#include <string>
#include <iostream>

Devkit::Devkit(std::string n, std::string d, int i, float p, float pm, int pa) : Item(n, d, i, p)
{
	profitMultiplier = pm;
	playerAmount = pa;
}

Devkit::Devkit() : Item(" ", " ", 0, 0)
{
	profitMultiplier = 0;
	playerAmount = 0;
}

void Devkit::Print()
{
	Item::Print();
	std::cout << "Profit multiplier from platform: " << GetProfitMultiplier() << std::endl;
	std::cout << "Player amount of platform: " << GetPlayerAmount() << std::endl;
}

void Devkit::SetPlayerAmount(int newPlayerAmount)
{
	playerAmount = newPlayerAmount;
}

void Devkit::SetProfitMultiplier(float newProfitMultiplier)
{
	profitMultiplier = newProfitMultiplier;
}

Devkit::~Devkit()
{
}
