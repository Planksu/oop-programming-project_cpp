#pragma once
#include "GameObject.h"
#include <string>
class Item : public GameObject
{
public:
	Item(std::string n, std::string d, int i, float p);
	Item();
	virtual ~Item();
	void Print();
	float getPrice() const { return price; }
	void SetPrice(float newPrice);

protected:
	float price;

};

