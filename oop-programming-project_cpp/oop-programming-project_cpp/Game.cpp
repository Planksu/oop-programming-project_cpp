#include "Game.h"
#include <iostream>
#include <time.h>

#define RANDOM rand() % 100000 + 4900000

Game::Game(std::string n, std::string d, int i, float p, Devkit* de, bool mp, std::string en) : Item(n, d, i, p)
{
	devkit = de;
	multiplayer = mp;
	engine = en;
}


Game::~Game()
{
}

void Game::Print()
{
	Item::Print();
	std::cout << "Profit multiplier from platform: " << devkit->GetProfitMultiplier() << std::endl;
	std::cout << "Player amount of platform: " << devkit->GetPlayerAmount() << std::endl;
	std::cout << std::boolalpha;
	std::cout << "Multiplayer: " << multiplayer <<  std::endl;
	std::cout << "Engine used: " << engine <<  std::endl;
}

float Game::CalculateProfits()
{
	float profit;

	profit = (devkit->GetPlayerAmount() * devkit->GetProfitMultiplier());

	return profit;
}