#include <iostream>
#include "MainGame.h"
#include "EventManager.h"
#include "Character.h"
#include "Game.h"
#include <vector>

#define STARTCASH 10000

int main()
{
	std::vector<Event*> events;
	std::vector<Character*> employees;
	std::vector<Character*> employeesInMarket;
	std::vector<Devkit*> devkits;
	std::vector<Game*> games;

	Devkit* steamKit = new Devkit("Steam Devkit", "A development kit for Steam", 1, 0, 0.5, 100000);
	devkits.push_back(steamKit);

	Market market1 = Market(devkits, employeesInMarket);

	Character *dude1 = new Character("John", "A programmer from a large game development company", 1, 10, 1, 6, 8, 3000);
	Character *dude2 = new Character("Duanne", "A marketing student from a local university, little experience", 2, 0, 0, 8, 1, 1800);

	employees.push_back(dude1);
	employees.push_back(dude2);

	FileHandler* filehandler = new FileHandler("games.txt", "devkits.txt", "employees.txt", "cash.txt");
	EventManager *eManager = new EventManager(events);
	MainGame game = MainGame(eManager, employees, devkits, games, STARTCASH, market1, filehandler);
	game.Start();
}