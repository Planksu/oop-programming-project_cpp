#ifndef FILEHANDLER_H
#define FILEHANDLER_H

#include <string>
#include <vector>

// Korjaa tarvittaessa oman luokkasi nimi t�h�n:
#include "MainGame.h"

/*****************************************************************************************************
FileHandler on apuluokka, joka hoitaa tiedostosta lukemisen ja tiedostoon kirjoittamisen.
Kaikki tieto kirjoitetaan tekstimuotoisena, 1 tieto/rivi.
Luo luokasta olio ja anna parametrina kummankin tiedoston nimi. Tiedostot luodaan automaattisesti,
jos niit� ei viel� ole olemassa.

Luokka lukee ja kirjoittaa kahta tiedostoa. Toinen tiedosto on ponin tietoja varten.
Ponin saamat palkinnot ja ponin omistamat tavarat tallennetaan tiedostoon pelkill� id-numeroilla.
Voit hakea tavaroiden id-numerot erillisell� metodilla (getItemIDs())
Metodi ylikirjoittaa olemassa olevan tiedoston tai luo uuden tiedoston, jos sit� ei viel� ole olemassa.

Ponin saamat palkinnot kirjoitetaan erilliseen tiedostoon. Metodi kirjoittaa uusien palkintojen
tiedot olemassa olevan tiedoston loppuun tai luo uuden tiedoston, jos tiedostoa ei viel� ole olemassa.
*/
class FileHandler
{
public:
	// Konstruktori
	// Parametreina kahden tiedoston nimet. Tiedostot luodaan, jos niit� ei viel�
	// ole olemassa
	FileHandler(std::string gamesFileName, std::string devkitsFileName, std::string employeeFileName, std::string cashFileName );

	// Metodit tietojen tallentamiseen:
	void saveItems(Game* game);
	void saveItems(Devkit* devkit);
	void saveItems(Character* employee);
	void saveCash(float cash);
	
	// Metodit tiedostojen lukemiseen:
	std::vector<Game*> loadGames();
	std::vector<Devkit*> loadDevkits();
	std::vector<Character*> loadEmployees();
	float loadCash();

private:
	// Ponin tiedot ja tavaroiden id:t kirjoitetaan omaan tiedostoon
	// Saatujen palkintojen tiedot kirjoitetaan toiseen tiedostoon
	std::string gamesFileName;
	std::string devkitsFileName;
	std::string employeesFileName;
	std::string cashFileName;
	
};


#endif
