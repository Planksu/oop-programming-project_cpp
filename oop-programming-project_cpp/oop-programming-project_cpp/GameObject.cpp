#include "GameObject.h"
#include <string>
#include <iostream>


GameObject::GameObject(std::string n, std::string d, int i)
{
	name = n;
	details = d;
	id = i;
}

//Default constructor
GameObject::GameObject()
{
	name = "";
	details = "";
	id = 0;
}

//Copy constructor
GameObject::GameObject(GameObject& other)
{
	name = other.name;
	details = other.details;
	id = other.id;
}

GameObject::~GameObject()
{
}

void GameObject::Print()
{
	std::cout << "Name: " << name << std::endl;
	std::cout << "Details: " << details << std::endl;
	std::cout << "ID: " << id << "\n" << std::endl;
}

void GameObject::SetName(std::string newName)
{
	name = newName;
}

void GameObject::SetDetails(std::string newDetails)
{
	details = newDetails;
}

void GameObject::SetID(int newId)
{
	id = newId;
}