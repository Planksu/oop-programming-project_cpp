#include "FileHandler.h"

#include <fstream>
#include <iostream>
#include <time.h>


// Konstruktorin toteutus
FileHandler::FileHandler(std::string gFileName, std::string dFileName, std::string eFileName, std::string cFileName)
{
	devkitsFileName = dFileName;
	gamesFileName = gFileName;
	employeesFileName = eFileName;
	cashFileName = cFileName;
}

// Metodi tallentaa yksittäisen uuden palkinnon tiedot
// tiedoston loppuun. Tiedosto avataan append-muodossa,
// olemassa olevia palkintoja ei ylikirjoiteta
void FileHandler::saveItems( Game* game )
{
	std::ofstream myFile(gamesFileName, std::ios::app );

	if (myFile)
	{
		myFile << game->getName() << std::endl;
		myFile << game->getDetails() << std::endl;
		myFile << game->getPrice() << std::endl;
		myFile << game->getId() << std::endl;

		myFile.close();
	}
}
void FileHandler::saveItems(Devkit* devkit)
{
	std::ofstream myFile(devkitsFileName, std::ios::app);

	if (myFile)
	{
		myFile << devkit->getName() << std::endl;
		myFile << devkit->getDetails() << std::endl;
		myFile << devkit->getPrice() << std::endl;
		myFile << devkit->getId() << std::endl;

		myFile.close();
	}
}
void FileHandler::saveItems(Character* employee)
{
	std::ofstream myFile(employeesFileName, std::ios::app);

	if (myFile)
	{
		myFile << employee->getName() << std::endl;
		myFile << employee->getDetails() << std::endl;
		myFile << employee->GetRequiredPay() << std::endl;
		myFile << employee->getId() << std::endl;

		myFile.close();
	}
}

void FileHandler::saveCash(float cash)
{
	std::ofstream myFile(cashFileName, std::ios::app);

	if (myFile)
	{
		myFile << cash << std::endl;
		myFile.close();
	}
}

// Metodi lukee palkintojen tiedot tiedostosta ja palauttaa
// ne vektorissa
std::vector<Devkit*> FileHandler::loadDevkits()
{
	std::vector<Devkit*> devkits;

	// Väliaikaiset muuttujat, joita tarvitaan tiedon lukemisessa
	std::string name, details;
	int id;
	float price;
	float profitMultiplier;
	float playerAmount;

	// Avataan tiedosto
	std::ifstream myFile( devkitsFileName);

	if (myFile)
	{
		while( myFile.peek() != EOF )
		{
			// Luetaan attribuuttien arvot...
			getline( myFile, name, '\n');
			getline( myFile, details, '\n');
			myFile >> price;
			myFile >> id;
			myFile >> profitMultiplier;
			myFile >> playerAmount;

			// .. luodaan Item-olio
			Devkit* devkit = new Devkit( name, details, price, id, profitMultiplier, playerAmount );		
			devkits.push_back(devkit);

			// Hävitetään rivinvaihto puskurista
			myFile.ignore();
			
		}

		myFile.close();
	}	

	return devkits;
}

std::vector<Game*> FileHandler::loadGames()
{
	std::vector<Game*> games;

	// Väliaikaiset muuttujat, joita tarvitaan tiedon lukemisessa
	std::string name, details;
	int id;
	float price;
	bool multiplayer;
	std::string engine;

	// Avataan tiedosto
	std::ifstream myFile(gamesFileName);

	if (myFile)
	{
		while (myFile.peek() != EOF)
		{
			// Luetaan attribuuttien arvot...
			getline(myFile, name, '\n');
			getline(myFile, details, '\n');
			myFile >> price;
			myFile >> id;
			myFile >> multiplayer;
			myFile >> engine;

			// .. luodaan Item-olio
			Game* game = new Game(name, details, price, id, new Devkit, multiplayer, engine);
			games.push_back(game);

			// Hävitetään rivinvaihto puskurista
			myFile.ignore();

		}

		myFile.close();
	}

	return games;
}

std::vector<Character*> FileHandler::loadEmployees()
{
	std::vector<Character*> employees;

	// Väliaikaiset muuttujat, joita tarvitaan tiedon lukemisessa
	std::string name, details;
	int id;
	float price;
	int programmingSkills;
	int graphicsSkills;
	int marketingSkills;
	int experienceYears;
	int requiredPay;

	// Avataan tiedosto
	std::ifstream myFile(devkitsFileName);

	if (myFile)
	{
		while (myFile.peek() != EOF)
		{
			// Luetaan attribuuttien arvot...
			getline(myFile, name, '\n');
			getline(myFile, details, '\n');
			myFile >> id;
			myFile >> programmingSkills;
			myFile >> graphicsSkills;
			myFile >> marketingSkills;
			myFile >> experienceYears;
			myFile >> requiredPay;

			// .. luodaan Item-olio
			Character* employee = new Character(name, details, id, programmingSkills, graphicsSkills, marketingSkills, experienceYears, requiredPay);
			employees.push_back(employee);

			// Hävitetään rivinvaihto puskurista
			myFile.ignore();

		}

		myFile.close();
	}

	return employees;
}

float FileHandler::loadCash()
{
	float cash;

	std::ifstream myFile(devkitsFileName);

	if (myFile)
	{
		while (myFile.peek() != EOF)
		{
			myFile >> cash;

			// Hävitetään rivinvaihto puskurista
			myFile.ignore();

		}

		myFile.close();
	}

	return cash;
}