#include "Character.h"
#include <string>
#include <iostream>

Character::Character(std::string n, std::string d, int i, int pS, int gS, int mS, int ey, int pay) : GameObject(n, d, i)
{
	programmingSkills = pS;
	graphicsSkills = gS;
	marketingSkills = mS;
	experienceYears = ey;
	requiredPay = pay;
}

Character::Character() : GameObject(" ", " ", 0)
{
	programmingSkills = 0;
	graphicsSkills = 0;
	marketingSkills = 0;
	experienceYears = 0;
}

Character::Character(Character& other)
{
	programmingSkills = other.programmingSkills;
	graphicsSkills = other.graphicsSkills;
	marketingSkills = other.marketingSkills;
	experienceYears = other.experienceYears;
}

void Character::Print()
{
	GameObject::Print();
	std::cout << "Programming skills: " << programmingSkills << std::endl;
	std::cout << "Graphics skills: " << graphicsSkills << std::endl;
	std::cout << "Marketing skills: " << marketingSkills << std::endl;
	std::cout << "Experience: " << experienceYears << std::endl;
	std::cout << "Required pay: " << requiredPay << "\n" << std::endl;
}

void Character::SetProgrammingSkills(int newProgrammingSkills)
{
	programmingSkills = newProgrammingSkills;
}

void Character::SetGraphicsSkills(int newGraphicsSkills)
{
	graphicsSkills = newGraphicsSkills;
}

void Character::SetMarketingSkills(int newMarketingSkills)
{
	marketingSkills = newMarketingSkills;
}

void Character::SetExperienceYears(int newExperienceYears)
{
	experienceYears = newExperienceYears;
}

Character::~Character()
{
}
