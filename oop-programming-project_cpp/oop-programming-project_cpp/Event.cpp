#include "Event.h"
#include <string>
#include <iostream>

Event::Event(std::string n, std::string d, int i)
{
	name = n;
	description = d;
	id = i;
}

//Default constructor
Event::Event()
{
	name = "";
	description = "";
	id = 0;
}

//Copy constructor
Event::Event(Event& other)
{
	name = other.name;
	description = other.description;
	id = other.id;
}

Event::~Event()
{
}

void Event::print()
{
	std::cout << "Name: " << name << std::endl;
	std::cout << "Description: " << description << std::endl;
	std::cout << "Id: " << id << std::endl;
}

void Event::SetName(std::string newName)
{
	name = newName;
}

void Event::SetDesc(std::string newDesc)
{
	description = newDesc;
}

void Event::SetId(int newId)
{
	id = newId;
}
